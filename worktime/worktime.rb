module Worktime
  extend self

  W_DAYS = %w(mon tue wed thu fri sat sun)
  W_SHORT_DAYS = %w(пн вт ср чт пт сб вс)
  W_FULL_DAYS_HASH = Hash[W_DAYS.zip(W_SHORT_DAYS)]

  W_DAYS_INDEX = %w(0 1 2 3 4 5 6)
  W_FULL_DAYS_INDEX = Hash[W_DAYS.zip(W_DAYS_INDEX)]
  W_FULL_DAYS_INDEX_REVERT = Hash[W_DAYS_INDEX.zip(W_DAYS)]

  def five_post(worktime_array)

    normalized_array = worktime_array.map do |day|
      {
          day: day['day'].downcase,
          short_day: W_FULL_DAYS_HASH[day['day'].downcase],
          opens_at: day['opensAt'],
          closes_at: day['closesAt'],
          index: W_FULL_DAYS_INDEX[day['day'].downcase]
      }

    end

    normalize(normalized_array)
  end

  def apiship(worktime)

    normalized_array = worktime.map do |key, value|
      {
          day: W_FULL_DAYS_INDEX_REVERT["#{key.to_i - 1}"],
          short_day: W_FULL_DAYS_HASH[W_FULL_DAYS_INDEX_REVERT["#{key.to_i - 1}"]],
          opens_at: value.split(/\//)[0],
          closes_at: value.split(/\//)[1],
          index: key.to_i - 1
      }

    end

    normalize(normalized_array)
  end

  private

  def normalize(normalized_array)

    if (normalized_array.map {|el| el[:opens_at]}.uniq.count == 1) && (normalized_array.map {|el| el[:closes_at]}.uniq.count == 1)
      # работает ежедневно в одно и тоже время
      "Ежедневно: #{Time.parse(normalized_array.first[:opens_at]).strftime('%H:%M')} - #{Time.parse(normalized_array.first[:closes_at]).strftime('%H:%M')}"
    else
      result_string = ''
      while normalized_array.count != 0
        selected_days = group_by_working_time(normalized_array)
        selected_days_indexes = selected_days.map {|day| day[:index]}

        unless result_string.empty?
          result_string += ', '
        end

        if selected_days_indexes.each_cons(2).all? {|a, b| b == a + 1 } && selected_days.count != 1
          result_string += "#{selected_days.select{|day| day[:index] == selected_days_indexes.min}.first[:short_day].capitalize }-#{selected_days.select{|day| day[:index] == selected_days_indexes.max}.first[:short_day]}: #{selected_days.select{|day| day[:index] == selected_days_indexes.max}.first[:opens_at]} - #{selected_days.select{|day| day[:index] == selected_days_indexes.max}.first[:closes_at]}"
        else
          if selected_days.count == 1
            selected_days.each do |day|
              result_string += "#{day[:short_day].capitalize}: #{day[:opens_at]} - #{day[:closes_at]}"
            end
          else
            splitted_days = ''
            selected_days.each_with_index do |day, index|
              splitted_days += if index == 0
                                 day[:short_day].capitalize
                               else
                                 day[:short_day]
                               end

              if index >= 0 && index < selected_days.count - 1
                splitted_days += ', '
              end
            end
            splitted_days += ": #{selected_days.first[:opens_at]} - #{selected_days.first[:closes_at]}"
            result_string += splitted_days
          end
        end

        normalized_array.delete_if {|day| selected_days.include?(day)}
      end
      result_string
    end

  end

  def group_by_working_time(days)
    return days if days.size == 1

    start_day_index = days.map{ |day| day[:index] }.min
    start_day = days.select { |day| day[:index] == start_day_index }.first
    selected_days = days.select { |day| day[:opens_at] == start_day[:opens_at] && day[:closes_at] == start_day[:closes_at] }
  end

end
