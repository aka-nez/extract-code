module Aliexpress
  extend self

  # https://developers.aliexpress.com/en/doc.htm?docId=42270&docType=2
  # если у метода есть вложенные параметры, их нужно передавать в виде строки, иначе не проходит проверка подписи
  def orders_list(status, current_page = 1, create_date = Time.now - 3.days)
    statuses = Oj.dump(status, mode: :json)
    params = {
        method: 'aliexpress.solution.order.get',
        param0: "{ 'order_status_list': #{statuses}, 'page_size': 100, 'current_page': '#{current_page}', 'create_date_start': '#{create_date.strftime('%Y-%m-%d %H:%M:%S')}' }"
    }

    result = send(params)
  end

  # https://developers.aliexpress.com/en/doc.htm?docId=42707&docType=2
  # order_id - id заказа на aliexpress
  def order_detail(order_id)
    params = {
        method: 'aliexpress.solution.order.info.get',
        param1: "{ 'order_id': '#{order_id}' }"

    }

    result = send(params)
  end

  # https://developers.aliexpress.com/en/doc.htm?spm=a219a.7386653.0.0.285c9b719Nbysl&docId=42269&docType=2
  # order - заказ в нашей системе
  def order_fullfil(order_id)
    order = Order.find(order_id)
    params = {
        method: 'aliexpress.solution.order.fulfill',
        service_name: order.delivery_info["ali_logistics_type"],
        tracking_website: 'https://cdek.ru/tracking',
        out_ref: order.external_order_id,
        send_type: 'all',
        logistics_no: order.delivery_info['cdek_track'][/\d+/]
    }

    result = send(params)
  end

  # https://developers.aliexpress.com/en/doc.htm?docId=35300&docType=2
  def get_group_product_list
    params = {
        method: 'aliexpress.product.productgroups.get'
    }

    result = send(params)
  end

  # https://developers.aliexpress.com/en/doc.htm?docId=30184&docType=2
  # product_id - id товара на aliexpress
  # group_ids - id товарных категорий внутри магазина на алиэкспресс
  def set_product_group(product_id, group_ids)
    params = {
        method: 'aliexpress.postproduct.redefining.setgroups',
        product_id: product_id,
        group_ids: group_ids
    }

    result = send(params)
  end

  # https://developers.aliexpress.com/en/doc.htm?docId=42384&docType=2
  # statuses: onSelling; offline; auditing; and editingRequired
  def get_product_list(sku, status,  page = 1)
    params = {
        method: 'aliexpress.solution.product.list.get',
        aeop_a_e_product_list_query: "{ 'current_page': '#{page}', 'product_status_type': '#{status}', 'sku_code': '#{sku}' }"
    }

    result = send(params)
  end

  # https://developers.aliexpress.com/en/doc.htm?docId=45135&docType=2
  # products[]: {product_id: integer, sku: string, stock: integer}
  # maximum 20 products per request
  def stock_update(products)

    product_list = '['
    products.each do |product|
      product_list << "{ 'product_id': '#{product[:product_id]}', 'multiple_sku_update_list': { 'sku_code': '#{product[:sku]}', 'inventory': '#{product[:stock]}' }}, "
    end
    product_list << ']'

    params = {
        method: 'aliexpress.solution.batch.product.inventory.update',
        mutiple_product_update_list: product_list
    }

    result = send(params)
  end

  def price_update(products)

    product_list = '['
    products.each do |product|
      product_list << "{ 'product_id': '#{product[:product_id]}', 'multiple_sku_update_list': { 'sku_code': '#{product[:sku]}', 'price': '#{product[:price]}', 'discount_price': '#{product[:discount_price]}' }}, "
    end
    product_list << ']'

    params = {
        method: 'aliexpress.solution.batch.product.price.update',
        mutiple_product_update_list: product_list
    }

    result = send(params)
  end

  private

  def send(params)
    # add method to required params
    request_params = required_params.merge(params)

    #generate request signature
    sign = sign(request_params)
    total_request_params = request_params.merge({sign: sign})

    response = RestClient.post(base_url, total_request_params, headers)
    JSON.parse(response.body)
  end

  # get list of required params for every api request (part of public params in ali api)
  def required_params
    {
        # method
        app_key: app_key,
        session: ali_session,
        timestamp: timestamp,
        format: format,
        v: version,
        sign_method: sign_method
    }
  end

  # https://developers.aliexpress.com/en/doc.htm?docId=108974&docType=1
  def sign(params)
    # sort by hash by key
    sorted_params_array = params.sort_by(&:first)

    # map params into flat string
    spliced_params = "#{app_secret}#{sorted_params_array.map { |el| "#{el.first}#{el.last}" }.join}#{app_secret}"

    # return the md5 signature
    sign = Digest::MD5.hexdigest(spliced_params).upcase
  end

  def base_url
    # production
    'https://api.taobao.com/router/rest'
  end

  def headers
    #'Content-Type:application/x-www-form-urlencoded;charset=utf-8'
    {
        content_type: 'application/x-www-form-urlencoded',
        charset: 'utf-8'
    }
  end

  def version
    '2.0'
  end

  def format
    'json'
  end

  def sign_method
    'md5'
  end

  def simplify
    true
  end

  def timestamp
    # ActiveSupport::TimeZone[+8] -> Beijing
    Time.zone = 'Beijing'
    Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')
  end

  def app_key
    SiteConfig['ali_app_key']
  end

  def app_secret
    SiteConfig['ali_app_secret']
  end

  def ali_session
    SiteConfig['ali_test_access_token']
  end

end