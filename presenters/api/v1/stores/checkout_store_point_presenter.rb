class Api::V1::Stores::CheckoutStorePointPresenter < Api::V1::BaseStorePointPresenter

  def initialize(targets, items)
    @targets = targets
    @items = items
    @delivery_dates = get_delivery_dates
  end

  def execute
    targets.map do |target|
      {
          id: target_id(target),
          type: target_type(target),
          slug: slug,
          title: title(target),
          address: address(target),
          metro_color: metro_color(target),
          worktime: worktime(target),
          cost: cost,
          delivery: delivery_date(target),
          lat_lng: lat_lng(target)
      }
    end
  end

  private

  attr_reader :targets, :items, :delivery_dates

  def delivery_date(target)
    delivery_dates[target.id]
  end

  def get_delivery_dates
    response = Api::V1::ItemServices::StoresDeliveryDate.call(targets, items)

    if response.success?
      response.data
    end
  end

end