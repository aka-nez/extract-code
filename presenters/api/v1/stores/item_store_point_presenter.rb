class Api::V1::Stores::ItemStorePointPresenter < Api::V1::BaseStorePointPresenter

  def initialize(targets, item)
    @targets = targets
    @item = item
    @item_availabilities = get_item_availabilities
  end

  def execute
    targets.map do |target|
      {
          id: target_id(target),
          type: target_type(target),
          slug: slug,
          title: title(target),
          address: address(target),
          metro_color: metro_color(target),
          worktime: worktime(target),
          cost: cost,
          delivery: delivery_date(target),
          available: available(target),
          lat_lng: lat_lng(target)
      }
    end
  end

  private

  attr_reader :targets, :item, :item_availabilities

  def available(target)
    item_availabilities[target.id] || 0
  end

  def get_item_availabilities
    result = {}
    Availability.where(store_id: targets.pluck(:id), item_variation_id: item.item_variations.first.id).each do |av|
      result[av.store_id] = av.quantity
    end
    result
  end

  def delivery_date(target)
    target.pickup_date(item_availabilities[target.id].nil?)
  end

end