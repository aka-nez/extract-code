class Api::V1::BaseStorePointPresenter < BasePresenter

  private

  def slug
    'pickup'
  end

  def title(target)
    "м. #{metro(target).title}"
  end

  def address(target)
    target.api_address
  end

  def metro(target)
    target.metro
  end

  def worktime(target)
    target.hours.capitalize
  end

  def cost
    0
  end

  def lat_lng(target)
    [target.lat, target.lng]
  end

  def metro_color(target)
    metro(target).color.present? ? metro(target).color : '46c3d2'
  end

end