class BasePresenter

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(targets)
    @targets = targets
  end

  private

  attr_reader :targets

  def target_id(target)
    target.id
  end

  def target_type(target)
   target.class.name.underscore
  end

end
