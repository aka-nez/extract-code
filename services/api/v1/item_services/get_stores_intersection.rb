class Api::V1::ItemServices::GetStoresIntersection < BaseService

  def initialize(items, city_id, args = {})
    @items = items
    @city_id = city_id
    @args = args
  end

  def execute
    array_of_available_stores = available_stores_ids

    if array_of_available_stores.count > 1
      longest_subarray_of_stores = array_of_available_stores.max_by(&:size)
      array_of_available_stores.delete_if { |el| el == longest_subarray_of_stores }
      result = longest_subarray_of_stores.intersection(*array_of_available_stores)
    else
      result = array_of_available_stores
    end

    stores = Store.where(id: result)

    if stores.present?
      success_response(stores)
    else
      error_response('Stores intersection not found', 10)
    end
  end

  private

  attr_reader :items, :city_id, :args

  def available_stores_ids
    stores = items.map do |item|
      response = Api::V1::ItemServices::GetStores.call(item, city_id)

      if response.success?
        response.data.pluck(:id)
      else
        []
      end
    end
  end

end