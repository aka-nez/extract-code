class Api::V1::ItemServices::GetStores < BaseService

  def initialize(item, city_id, args = {})
    @item = item
    @city_id = city_id
    @args = args
  end

  def execute
    ids = item.availabilities.joins(:store).
        where(stores: { city_id: city_id, reserve_available: true, hidden: false}).
        where.not(store_id: Store.central_warehouses.pluck(:id)).
        distinct.pluck(:store_id)

    if available_in_central_wh?
      ids << Store.where(from_central_warehouse: true, reserve_available: true, hidden: false, city_id: city_id).pluck(:id)
    end

    stores = Store.where(
        id: ids.flatten
    )

    success_response(stores)
  end

  private

  attr_reader :item, :city_id, :args

  def available_in_central_wh?
    iv = item.item_variations.first.id
    if item.availabilities.where(store_id: Store.central_warehouses.pluck(:id), item_variation_id: iv).present?
      true
    else
      false
    end
  end

end