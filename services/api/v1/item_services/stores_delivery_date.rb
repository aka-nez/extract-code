class Api::V1::ItemServices::StoresDeliveryDate < BaseService

  def initialize(stores, items_with_quantity)
    @stores = stores.includes(:store_supplies)
    @items = items_with_quantity # [[Item, quantity], ...]
    @items_count = items_with_quantity.count
    @current_time = Time.current
  end

  def execute
    store_supplies = near_store_supply

    return error_response('Store supply not found', 10) unless store_supplies.present?

    success_response(store_supplies)
  end

  private

  attr_reader :stores, :items, :items_count, :current_time

  def near_store_supply
    result = {}

    availability_info.each do |info|
      store_id = info['store_id']
      av_count = info['av_count']

      if all_items_available_in_store?(av_count)
        result[store_id] = start_time_with_overhour_limit
      else
        result[store_id] = next_store_supply(store_id)
      end
    end

    result
  end

  def availability_info
    response = ActiveRecord::Base.connection.select_all(generate_sql).to_a
  end

  def all_items_available_in_store?(qty)
    items_count == qty
  end

  def over_hour_limit
    SiteConfig['current_day_delivery_over_hour']
  end

  def start_time_with_overhour_limit
    start_time = if current_time.strftime('%H').to_i >= over_hour_limit
                   current_time.beginning_of_day + 2.day
                 else
                   current_time.beginning_of_day + 1.day
                 end
  end

  def next_store_supply(store_id)
    supplies = StoreSupply.where('supply_date >= ? and store_id = ?', (start_time_with_overhour_limit).beginning_of_day, store_id).order('supply_date ASC')

    if supplies.size != 0
      supplies.first.supply_date
    else
      current_time + 4.days
    end
  end

  def generate_sql
    sql = "select availabilities.store_id as store_id, " \
          "COUNT(distinct availabilities.item_variation_id) as av_count " \
          "from availabilities inner join stores on availabilities.store_id = stores.id " \
          "where availabilities.store_id in (#{stores.map(&:id).map{|el| "'#{el}'"}.join(",")}) " \
          "and (#{conditions}) " \
          "group by availabilities.store_id"
  end

  def conditions
    condition = ''
    counter = 0
    items.each do |item, quantity|
      condition << " OR " if counter > 0
      condition << "(availabilities.item_variation_id = #{item.item_variations.first.id} and availabilities.quantity >= #{quantity} )"
      counter += 1
    end
    condition
  end

end