class BaseService

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  private

  def success_response(data)
    OpenStruct.new({success?: true, data: data})
  end

  def error_response(error, code)
    OpenStruct.new({success?: false, code: code, error: error})
  end

end